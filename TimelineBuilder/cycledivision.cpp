#include "cycledivision.h"

CycleDivision::CycleDivision(QString name, int numUnits)
{
    m_name = name;
    m_numUnits = numUnits;
}

CycleDivision::~CycleDivision()
{
    // Nothing to do here for now
}

void CycleDivision::setName(QString name)
{
    //
    m_name = name;
}

QString CycleDivision::getName()
{
    return m_name;
}

void CycleDivision::setNumUnits(int num)
{
    m_numUnits = num;
}

int CycleDivision::getNumUnits()
{
    return m_numUnits;
}
