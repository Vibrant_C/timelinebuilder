#ifndef CYCLEDIVISION_H
#define CYCLEDIVISION_H

#include <QWidget>

class CycleDivision
{

public:
    CycleDivision(QString name, int numUnits);

    ~CycleDivision();

    void setName(QString name);

    QString getName();

    void setNumUnits(int num);

    int getNumUnits();

private:
    int m_numUnits;
    QString m_name;

};

#endif // CYCLEDIVISION_H
