#ifndef CYCLE_H
#define CYCLE_H

#include <QWidget>
#include <cycledivision.h>

class Cycle
{
public:
    Cycle(int numUnits);

    int getNumUnits();

private:
   int m_numUnits;
   QList<CycleDivision> Divisions;
};

#endif // CYCLE_H
