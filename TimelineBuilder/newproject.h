#ifndef NEWPROJECT_H
#define NEWPROJECT_H

#include <QDialog>

#include <calendar.h>
#include <cycle.h>
#include <cycledivision.h>

namespace Ui {
class NewProject;
}

class NewProject : public QDialog
{
    Q_OBJECT

public:
    explicit NewProject(QWidget *parent = nullptr);
    ~NewProject();

private slots:
    void on_chkDivided_stateChanged(int arg1);

    void on_clearBtn_clicked();

    void on_cancelBtn_clicked();

    void on_okBtn_clicked();

    void on_nextBtn_clicked();

private:
    Ui::NewProject *ui;

    QString m_cycleFormat;
    QString m_unitFormat;

    QList<CycleDivision> m_divisions;
};

#endif // NEWPROJECT_H
