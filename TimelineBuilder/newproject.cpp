#include "newproject.h"
#include "ui_newproject.h"

NewProject::NewProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProject)
{
    ui->setupUi(this);
}

NewProject::~NewProject()
{
    delete ui;
}

void NewProject::on_chkDivided_stateChanged(int arg1)
{
    ui->divTree->setEnabled(arg1);
}

void NewProject::on_clearBtn_clicked()
{
    ui->cbCalendarStyle->clear();
    ui->txtUnitFormat->clear();
    ui->txtCycleFormat->clear();
    ui->divTree->clear();
    ui->okBtn->setEnabled(false);
    ui->clearBtn->setEnabled(false);
}

void NewProject::on_cancelBtn_clicked()
{
    this->close();
}

void NewProject::on_nextBtn_clicked()
{

}
