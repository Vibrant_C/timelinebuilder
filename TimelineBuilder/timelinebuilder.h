#ifndef TIMELINEBUILDER_H
#define TIMELINEBUILDER_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class TimelineBuilder; }
QT_END_NAMESPACE

class TimelineBuilder : public QMainWindow
{
    Q_OBJECT

public:
    TimelineBuilder(QWidget *parent = nullptr);
    ~TimelineBuilder();

private:
    Ui::TimelineBuilder *ui;
};
#endif // TIMELINEBUILDER_H
